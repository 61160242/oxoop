/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class PlayerUnit {
    
    public PlayerUnit() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testLose()throws Exception{
        Player x =  new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        table.checkWin();
        table.switchTurn();
        assertEquals(1, table.getCurrentPlayer().getLose());
    }
    
    @Test
    public void testDraw() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(3, 3);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(1, 3);
        table.switchTurn();
        table.setRowCol(3, 1);
        table.switchTurn();
        table.setRowCol(3, 2);
        table.switchTurn();
        table.setRowCol(2, 3);
        table.checkDraw();
        assertEquals(1,table.getCurrentPlayer().getDraw());
    }
    @Test
    public void testWin() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        table.checkWin();
        assertEquals(1, table.getCurrentPlayer().getWin());
    }
    
    @Test
    public void testName(){
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        assertEquals('x',table.getCurrentPlayer().getName());
    }
    
}
