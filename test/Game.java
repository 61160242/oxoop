


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
import java.util.*;

public class Game {

    private Scanner kb = new Scanner(System.in);
    private int row;
    private int col;
    private Table table;
    private Player o;
    private Player x;

    public Game() {
        x = new Player('X');
        o = new Player('O');
    }

    public void showWin() {
        System.out.println("Player " + table.getCurrentPlayer().getName() + " win..");
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        System.out.println("Turn " + table.getCurrentPlayer().getName());
    }

    public void showBye() {
        System.out.println("Bye Bye...");
    }

    public void showTable() {
        for (int i = 0; i < table.getTable().length; i++) {
            for (int j = 0; j < table.getTable()[i].length; j++) {
                System.out.print(table.getTable()[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void inputRowCol() {
        try {

            System.out.print("Please input Row Col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            table.setRowCol(row, col);

        } catch (Exception e) {
            showWrong();
            kb.nextLine();
            showTurn();
            inputRowCol();
        }
    }

    public void startGame() {
        table = new Table(x, o);
    }

    public void run() {
        this.startGame();
        this.showWelcome();
        do {
            this.showTable();
            this.showTurn();
            this.runAgain();
            this.startGame();
        } while (inputContinue());
        this.showBye();
    }

    public boolean inputContinue() {
        System.out.println("Plase input continue (y/n) : ");
        char input = kb.next().charAt(0);
        while (input != 'y' && input != 'n') {
            input = kb.next().charAt(0);
        }
        if (input == 'y') {
            return true;
        }
        return false;
    }

    public static void showWrong() {
        System.out.println("Please input position again");
    }

    public void showDraw() {
        System.out.println("Draw!!");
    }

    public void runAgain() {
        while (true) {
            this.inputRowCol();
            this.showTable();
            if (table.checkWin()) {
                this.showWin();
                break;
            } else if (table.checkDraw()) {
                this.showDraw();
                break;
            }
            table.switchTurn();
            this.showTurn();
        }
    }
    
}
