
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class Table {

    private char[][] tableChar = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;

    public Table(Player x, Player o) {
        this.x = x;
        this.o = o;
        Random random = new Random();
        if(random.nextInt(2)+1==1){
            currentPlayer=x;
        }
        else{
            currentPlayer=o;
        }
    }
    public Player getWinPlayer() {
        return win;
    }

    public char[][] getTable() {
        return tableChar;
    }

    public void switchTurn() {
        if (currentPlayer == x) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean checkRow() {
        for (int row = 0; row < tableChar.length; row++) {
            if (tableChar[row][0] == tableChar[row][1]
                    && tableChar[row][0] == tableChar[row][2]
                    && tableChar[row][0] != '-') {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCol(){
        for (int col = 0; col < tableChar.length; col++) {
            if (tableChar[0][col] == tableChar[1][col]
                    && tableChar[0][col] == tableChar[2][col]
                    && tableChar[0][col] != '-') {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCross1(){
        for(int cross1 = 0;cross1<tableChar.length;cross1++){
            if(tableChar[0][0] == tableChar[1][1] 
                    && tableChar[1][1] == tableChar[2][2] 
                    && tableChar[0][0] != '-'){
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCross2(){
        for(int cross2 = 0;cross2 < tableChar.length; cross2++){
            if(tableChar[0][2] == tableChar[1][1] 
                    && tableChar[1][1] == tableChar[2][0] 
                    && tableChar[0][2] != '-'){
                return true;
            }
        }
        return false;
    }

    public boolean setRowCol(int row, int col) {
        if (this.getTable()[row-1][col-1] == '-') {
            tableChar[row-1][col-1] = currentPlayer.getName();
            return true;
        } else {
            return false;
        }
    }

    
    public boolean checkWin(){
        if(checkRow() || checkCol() || checkCross1() || checkCross2()){
            win = currentPlayer;
            if(currentPlayer == x){
                x.win();
                o.lose();
            }
            else{
                o.win();
                x.lose();
            }
            return true;
        }
        return false;
    }
    
    public boolean checkDraw(){
        int count = 0;
        for(int i=0; i<tableChar.length;i++){
            for(int j=0;j<tableChar.length;j++){
                 if(tableChar[i][j] != '-'){
                     count++;
                 }
            }
        }
        if(count == 9 && !checkWin()){
            x.graw();
            o.graw();
            return true;
        }
        return false;
    
    }
        


}
